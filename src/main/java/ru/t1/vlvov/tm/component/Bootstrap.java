package ru.t1.vlvov.tm.component;

import ru.t1.vlvov.tm.api.controller.ICommandController;
import ru.t1.vlvov.tm.api.controller.IProjectController;
import ru.t1.vlvov.tm.api.controller.ITaskController;
import ru.t1.vlvov.tm.api.repository.ICommandRepository;
import ru.t1.vlvov.tm.api.repository.IProjectRepository;
import ru.t1.vlvov.tm.api.repository.ITaskRepository;
import ru.t1.vlvov.tm.api.service.ICommandService;
import ru.t1.vlvov.tm.api.service.IProjectService;
import ru.t1.vlvov.tm.api.service.ITaskService;
import ru.t1.vlvov.tm.constant.ArgumentConst;
import ru.t1.vlvov.tm.constant.TerminalConst;
import ru.t1.vlvov.tm.controller.CommandController;
import ru.t1.vlvov.tm.controller.ProjectController;
import ru.t1.vlvov.tm.controller.TaskController;
import ru.t1.vlvov.tm.repository.CommandRepository;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import ru.t1.vlvov.tm.repository.TaskRepository;
import ru.t1.vlvov.tm.service.CommandService;
import ru.t1.vlvov.tm.service.ProjectService;
import ru.t1.vlvov.tm.service.TaskService;
import ru.t1.vlvov.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    public void run(String[] args) {
        if (processArguments(args)) exit();
        processCommands();
    }

    private boolean processArguments(final String args[]) {
        if (args == null || args.length == 0) return false;
        final String argument = args[0];
        processArgument(argument);
        return true;
    }

    private void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    private void processCommands() {
        System.out.println("** WELCOME TO TASK-MANAGER**");
        String command;
        while (true) {
            System.out.println("ENTER COMMAND");
            command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processCommand(final String command) {
        if (command == null) {
            commandController.showErrorCommand();
            return;
        }
        switch (command) {
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            case TerminalConst.INFO:
                commandController.showInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConst.UPDATE_PROJECT_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.UPDATE_PROJECT_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.REMOVE_PROJECT_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.REMOVE_PROJECT_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.SHOW_PROJECT_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.SHOW_PROJECT_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.UPDATE_TASK_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.UPDATE_TASK_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.REMOVE_TASK_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.REMOVE_TASK_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.SHOW_TASK_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.SHOW_TASK_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

    private void exit() {
        System.exit(0);
    }

}
