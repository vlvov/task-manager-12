package ru.t1.vlvov.tm.constant;

public final class TerminalConst {

    public final static String VERSION = "version";

    public final static String HELP = "help";

    public final static String ABOUT = "about";

    public final static String EXIT = "exit";

    public final static String INFO = "info";

    public final static String COMMANDS = "commands";

    public final static String ARGUMENTS = "arguments";

    public final static String PROJECT_CREATE = "project-create";

    public final static String PROJECT_CLEAR = "project-clear";

    public final static String PROJECT_LIST = "project-list";

    public final static String TASK_CREATE = "task-create";

    public final static String TASK_CLEAR = "task-clear";

    public final static String TASK_LIST = "task-list";

    public final static String UPDATE_PROJECT_BY_INDEX = "update-project-by-index";

    public final static String UPDATE_PROJECT_BY_ID = "update-project-by-id";

    public final static String REMOVE_PROJECT_BY_INDEX = "remove-project-by-index";

    public final static String REMOVE_PROJECT_BY_ID = "remove-project-by-id";

    public final static String SHOW_PROJECT_BY_INDEX = "show-project-by-index";

    public final static String SHOW_PROJECT_BY_ID = "show-project-by-id";

    public final static String UPDATE_TASK_BY_INDEX = "update-task-by-index";

    public final static String UPDATE_TASK_BY_ID = "update-task-by-id";

    public final static String REMOVE_TASK_BY_INDEX = "remove-task-by-index";

    public final static String REMOVE_TASK_BY_ID = "remove-task-by-id";

    public final static String SHOW_TASK_BY_INDEX = "show-task-by-index";

    public final static String SHOW_TASK_BY_ID = "show-task-by-id";

    public final static String PROJECT_CHANGE_STATUS_BY_ID = "project-change-status-by-id";

    public final static String PROJECT_CHANGE_STATUS_BY_INDEX = "project-change-status-by-index";

    public final static String PROJECT_COMPLETE_BY_ID = "project-complete-by-id";

    public final static String PROJECT_COMPLETE_BY_INDEX = "project-complete-by-index";

    public final static String PROJECT_START_BY_ID = "project-start-by-id";

    public final static String PROJECT_START_BY_INDEX = "project-start-by-index";

    public final static String TASK_CHANGE_STATUS_BY_ID = "task-change-status-by-id";

    public final static String TASK_CHANGE_STATUS_BY_INDEX = "task-change-status-by-index";

    public final static String TASK_COMPLETE_BY_ID = "task-complete-by-id";

    public final static String TASK_COMPLETE_BY_INDEX = "task-complete-by-index";

    public final static String TASK_START_BY_ID = "task-start-by-id";

    public final static String TASK_START_BY_INDEX = "task-start-by-index";

}
