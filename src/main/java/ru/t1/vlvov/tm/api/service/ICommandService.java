package ru.t1.vlvov.tm.api.service;

import ru.t1.vlvov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
