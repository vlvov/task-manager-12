package ru.t1.vlvov.tm.api.controller;

public interface ICommandController {

    void showCommands();

    void showArguments();

    void showInfo();

    void showVersion();

    void showAbout();

    void showHelp();

    void showErrorArgument();

    void showErrorCommand();

}
